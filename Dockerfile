FROM python:latest
WORKDIR /app
COPY server.py /app
COPY protos/ /app/protos/
COPY requirements.txt /app
RUN pip install -r requirements.txt
EXPOSE 8080
CMD ["python3", "server.py"]
