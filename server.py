from concurrent.futures.thread import ThreadPoolExecutor
import grpc

import logging

import time
from time import sleep

from random import shuffle
from protos import mafia_pb2_grpc, mafia_pb2

GRPC_CONNECTION_PORT = 8080


class MafiaHostService(mafia_pb2_grpc.MafiaHostServicer):
    def __init__(self):
        logging.info(f"Ожидаю подключения пользователей\nИгра начнётся когда подключатся хотя бы 4 игрока")
        self.users = {}
        self.minimum_players = 4
        self.round = 0
        self.roles = {
            "мафия" : [], 
            "комиссар" : [], 
            "мирный житель" : []
        }
        self.executed = []

    def AttachNewUserToLobby(self, request, context):
        logging.info(f"Подключился новый игрок: {request.username}")
        self.users[request.username] = []
        self.send_message_to_all_users(f"Подключился новый игрок: {request.username}")
        for current_user in self.users.keys():
            yield mafia_pb2.User(username=current_user)
        
        if (self.check_game_start()):
            self.send_message_to_all_users("Присоединилось достаточное количество для начала игры\nПриготовьтесь получить свою роль и приступить к игре")
            self.shuffle_roles()
            self.send_message_to_all_users("Игра началась!")

    def GetListOfUsers(self, request, context):
        for user in self.users.keys():
            yield mafia_pb2.User(username=user)
    
    def UserDisconnect(self, request, context):
        self.users.pop(request.username)
        self.send_message_to_all_users(f"{request.username} покинул игру, его роль не будет раскрыта")
        return mafia_pb2.Disconnected()

    def Activity(self, request, context):
        while True:
            if len(self.users[request.username]) == 0:
                sleep(5)
                continue
            msg = mafia_pb2.Msg(message=self.users[request.username].pop(0))
            yield msg

    def send_message_to_user(self, user, text):
        self.users[user].append(text)

    def send_message_to_all_users(self, text):
        for user in self.users:
            self.users[user].append(text)

    def check_game_start(self):
        return len(self.users.keys()) >= self.minimum_players
    
    def shuffle_roles(self):
        roles = ['мафия'] + ['комиссар'] + ['мирный житель'] * 2
        shuffle(roles)
        for user in self.users.keys():
            role = roles.pop()
            self.roles[role].append(user)
            self.send_message_to_user(user, f"Ты избран на роль {role}")
    
    def get_alive_players(self):
        return (set(self.roles['комиссар']) | set(self.roles['мирный житель']) - set(self.executed))


def start_polling(server):
    try:
        while 1:
            time.sleep(86400)
    except KeyboardInterrupt:
        server.stop(0)


def serve():
    server = grpc.server(ThreadPoolExecutor(max_workers=30))
    mafia_pb2_grpc.add_MafiaHostServicer_to_server(MafiaHostService(), server)
    server.add_insecure_port(f"[::]:{GRPC_CONNECTION_PORT}")
    server.start()
    logging.info("Инициализация сервера...")
    start_polling(server)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    serve()
