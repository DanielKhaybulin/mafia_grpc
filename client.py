import grpc

import logging

import threading
from protos import mafia_pb2_grpc, mafia_pb2


class MafiaClient:
    def __init__(self, address):
        self.channel = grpc.insecure_channel(address)
        self.stub = mafia_pb2_grpc.MafiaHostStub(self.channel)
        self.show_users()
        self.username = input("Введите уникальное имя под которым хотите подключиться к серверу:")
        self.attach_joined_user(self.username)
        self.activity = threading.Thread(target=self.activity)
        self.activity.daemon = True
        self.activity.start()
        self.start = False
        self.process()

    def process_client_message(self, msg):
        if msg == "userlist":
            self.show_users()
        if msg == "disconnect":
            self.disconnect()

    def process(self):
        while 1:
            self.process_client_message(input())

    def attach_joined_user(self, username):
        new_user = mafia_pb2.User(username=username)
        users = []
        for user in self.stub.AttachNewUserToLobby(new_user):
            users.append(user.username)
        logging.info(f"Игроки в игре: {users}")
        

    def show_users(self):
        request = mafia_pb2.UserListRequest()
        users = []
        for user in self.stub.GetListOfUsers(request):
            users.append(user.username)
        logging.info(f"Игроки, подключённые к лобби: {users}")
    
    def activity(self):
        request = mafia_pb2.User(username=self.username)
        for msg in self.stub.Activity(request):
            logging.info(f"{msg.message}")

            if self.check_game_start(msg.message):
                pass
    
    def disconnect(self):
        user = mafia_pb2.User(username=self.username)
        dc = self.stub.UserDisconnect(user)
        exit()
            
    def check_game_start(self, msg):
        if msg == "Игра началась!":
            self.start = True
        return self.start

    def __del__(self):
        logging.info("Игрок удалён.")


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    ip = input("Введите IP: ")
    host = input("Введите port: ")
    client = MafiaClient(f"{ip}:{host}")
